#!/bin/sh

# If the first arg starts with a hyphen, prepend clasp to arguments.
if [ "${1#-}" != "$1" ]; then
	set -- clasp "$@"
fi

exec "$@"
