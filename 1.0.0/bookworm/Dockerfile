#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#
FROM buildpack-deps:bookworm

# Install cl-launch.
# hadolint ignore=DL3008
RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y cl-launch \
    && rm -rf /var/lib/apt/lists/*

ENV LANG en_US.UTF-8

ENV CLASP_VERSION 1.0.0

WORKDIR /usr/local/src/

# hadolint ignore=DL3003,DL3008,DL4006
RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y sbcl libgc-dev libelf-dev libbsd-dev \
                                                  llvm-13 llvm-13-dev \
                                                  clang-13 libclang-13-dev \
                                                  libboost-dev \
                                                  locales locales-all \
    && git clone https://github.com/clasp-developers/clasp.git \
    && cd clasp \
    && git checkout $CLASP_VERSION \
    && echo "USE_PARALLEL_BUILD = True" >> wscript.config \
    && ./waf configure \
    && ./waf build_cboehm \
    && ./waf install_cboehm \
    && ./waf distclean \
    && ln -s iclasp-boehm /usr/local/bin/clasp \
    && apt-get remove -y sbcl \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && clasp --version

# Add the Quicklisp installer.
WORKDIR /usr/local/share/common-lisp/source/quicklisp/

ENV QUICKLISP_SIGNING_KEY D7A3489DDEFE32B7D0E7CC61307965AB028B5FF7

RUN set -x \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp" > quicklisp.lisp \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp.asc" > quicklisp.lisp.asc \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "${QUICKLISP_SIGNING_KEY}" \
    && gpg --batch --verify "quicklisp.lisp.asc" "quicklisp.lisp" \
    && rm quicklisp.lisp.asc \
    && rm -rf "$GNUPGHOME"

# Add the script to trivially install Quicklisp
COPY install-quicklisp /usr/local/bin/install-quicklisp

# Add the entrypoint
WORKDIR /

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["clasp"]
